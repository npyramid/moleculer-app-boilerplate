[![Moleculer](https://badgen.net/badge/Powered%20by/Moleculer/0e83cd)](https://moleculer.services)

# user-service

## NPM scripts

- `yarn dev`: Start development mode (load all services locally with hot-reload & REPL)
- `yarn start`: Start production mode (set `SERVICES` env variable to load certain services)
- `yarn lint`: Run ESLint
- `yarn test`: Run tests
- `yarn dc:up`: Start the stack with Docker Compose
- `yarn dc:down`: Stop the stack with Docker Compose
- `yarn nats:run`: Create and run nats container
