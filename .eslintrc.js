const globals = require('./alias');

const globalRules = Object.keys(globals).reduce((acc, key) => {
    acc[key] = 'readonly';
    return acc;
}, {});

module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    mocha: true,
  },
  extends: [
    'airbnb-base'
  ],
  globals: {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
    ...globalRules,
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  plugins: [
    'mocha',
  ],
  rules: {
    'mocha/no-exclusive-tests': 'error',
    'object-curly-newline': 'off',
    'import/no-dynamic-require': 'off',
  },
  root: true,
};
