const dbConfig = config('db');
const env = process.env.NODE_ENV || 'development';

const isProduction = env === 'production';
const isDevelopment = env === 'development';

const db = dbConfig[env];

module.exports = {
  isDevelopment,
  isProduction,
  db,
};
