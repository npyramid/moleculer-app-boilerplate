/* eslint-disable import/no-dynamic-require,global-require */
const path = require('path');

const globals = {};
globals.appRoot = __dirname;
globals.absolutePath = (...rest) => path.join(__dirname, ...rest);
globals.config = name => require(path.join(__dirname, 'config', name));

Object.entries(globals).forEach(([key, value]) => {
  global[key] = value;
});

module.exports = globals;
