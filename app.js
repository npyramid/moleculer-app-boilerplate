require('./alias');

const { ServiceBroker } = require('moleculer');

const config = require(absolutePath('config', 'brokerConfig'));
const services = require('require-all')(absolutePath('services'));

const broker = new ServiceBroker(config);

Object.values(services).forEach(serviceDir => broker.createService(serviceDir.index));

if (!module.parent) {
  broker.start().then(() => broker.logger.info('App started'));
}

module.exports = broker;
