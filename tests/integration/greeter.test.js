const broker = require(absolutePath('app'));
const { ValidationError } = require('moleculer').Errors;
const { assert } = require('chai');

describe("Test 'greeter' service", () => {
  describe("Test 'greeter.hello' action", () => {
    it('should return with "Hello Moleculer"', () => broker
      .call('greeter.hello')
      .then((result) => {
        assert(result === 'Hello Moleculer');
      }));
  });

  describe("Test 'greeter.welcome' action", () => {
    it("should return with 'Welcome'", () => broker
      .call('greeter.welcome', { name: 'Adam' })
      .then((result) => {
        assert(result === 'Welcome, Adam');
      }));

    it('should reject an ValidationError', () => broker
      .call('greeter.welcome')
      .then(() => Promise.reject())
      .catch((err) => {
        assert(err instanceof ValidationError);
        return Promise.resolve();
      }));
  });
});
